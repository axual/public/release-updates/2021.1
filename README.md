# Axual 2021.1 release update demos

In this repo you will find code starring in the 2021.1 release update. 

For the YouTube video, check https://youtu.be/hmLTv07RStg

## What do I learn from this demo?
By checking out the YouTube video of the 2021.1 release and following along with the examples, you get a better understanding on how you can leverage Kafka transactions to have better control over message processing and reduce duplicates in a streaming data pipeline.


## How to use the examples

1. Start the platform locally using `make compose`. This will compile the schemas used in the examples and start `docker-compose`.

2. Start the `incident-reporter` application.
    ```bash
    cd incident-reporter
    mvn clean package spring-boot:run
    ```
    After a while, the application will start producing messages to the `application-incidents` topic on the local platform.
    
3. Start the `incident-analyzer` application:
    ```bash
    cd incident-analyzer
    mvn clean package exec:java
    ```
    After a while, the application will start consuming messages from the `application-incidents` topic and filter and process events, producing to the `application-incidents-filtered` topic.

4. Start the `incident-alerter` application:
    ```bash
    cd incident-alerter
    mvn clean package exec:java
    ```
    After a while, the application will start consuming messages from the `application-incidents-filtered` topic and "alert" operators. 


## How to configure the applications

- Incident reporter: check the [`com.demo.operations.IncidentReporterProducerApplication`](incident-reporter/src/main/java/com/demo/operations/IncidentReporterProducerApplication.java) for producer configuration options.
- Incident analyzer: check the [`config.yml`](incident-analyzer/src/main/resources/config.yaml) for producer configuration options.
- Incident alerter: check the [`config.yml`](incident-alerter/src/main/resources/config.yaml) for producer configuration options.

# Contributing
Axual is interested in building the community; we would welcome any thoughts or patches.
See [CONTRIBUTING.MD](contributing.md)

# License
This 2021.1 release update demo project is licensed under the Apache License, Version 2.0.
