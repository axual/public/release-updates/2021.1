.PHONY: install-schemas
install-schemas:
	cd data-definitions && mvn clean package

.PHONY: compose
compose: install-schemas
	docker-compose up
