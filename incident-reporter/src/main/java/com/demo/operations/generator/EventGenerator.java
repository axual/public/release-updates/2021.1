package com.demo.operations.generator;

/**
 * Generates some object used for stubbing a producer
 */
public interface EventGenerator<K, V> {
    V generate();

    K getKey(V record);
}
