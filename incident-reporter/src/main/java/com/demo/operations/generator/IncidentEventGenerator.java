package com.demo.operations.generator;

import com.demo.operations.Application;
import com.demo.operations.ApplicationIncidentEvent;
import com.demo.operations.Severity;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.security.SecureRandom;

@Component
@ConfigurationProperties(prefix="generator")
public class IncidentEventGenerator implements EventGenerator<Application, ApplicationIncidentEvent> {

    private static final SecureRandom random = new SecureRandom();

    @Override
    public ApplicationIncidentEvent generate() {
        Application application = Application.newBuilder()
                .setName("random")
                .setOwner("Team Alpha")
                .setVersion("1.0.1")
                .build();

        return ApplicationIncidentEvent.newBuilder()
                .setIncidentId("INC-"+random.nextInt(99999))
                .setSeverity(randomEnum(Severity.class))
                .setMessage("Something serious happened in application 'random'")
                .setTimestamp(System.currentTimeMillis())
                .setApplication(application)
                .build();
    }

    @Override
    public Application getKey(ApplicationIncidentEvent record) {
        return record.getApplication();
    }

    private static <T extends Enum<?>> T randomEnum(Class<T> clazz){
        int x = random.nextInt(clazz.getEnumConstants().length);
        return clazz.getEnumConstants()[x];
    }
}
