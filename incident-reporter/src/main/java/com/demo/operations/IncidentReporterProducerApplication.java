package com.demo.operations;

import com.demo.operations.generator.IncidentEventGenerator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutionException;

import io.axual.client.AxualClient;
import io.axual.client.config.DeliveryStrategy;
import io.axual.client.config.OrderingStrategy;
import io.axual.client.config.SpecificAvroProducerConfig;
import io.axual.client.producer.ProduceCallback;
import io.axual.client.producer.ProducedMessage;
import io.axual.client.producer.Producer;
import io.axual.client.producer.ProducerMessage;
import io.axual.common.config.ClientConfig;
import io.axual.common.config.SslConfig;

@Component
public class IncidentReporterProducerApplication {
    private final static Logger LOG = LoggerFactory.getLogger(IncidentReporterProducerApplication.class.getName());

    private Producer<Application, ApplicationIncidentEvent> producer;
    private IncidentEventGenerator incidentEventGenerator;

    public IncidentReporterProducerApplication(@Autowired IncidentEventGenerator eventGenerator) {
        final AxualClient axualClient = new AxualClient(getClientConfig());

        producer = axualClient.buildProducer(getAvroProducerConfig());
        incidentEventGenerator = eventGenerator;
    }

    @Scheduled(fixedRate = 100)
    public void produce() {
        ApplicationIncidentEvent applicationIncidentEvent = incidentEventGenerator.generate();
        Application application = incidentEventGenerator.getKey(applicationIncidentEvent);

        ProducerMessage<Application, ApplicationIncidentEvent> accountEntryMessage = ProducerMessage.<Application, ApplicationIncidentEvent>newBuilder()
                .setStream("application-incidents")
                .setKey(application)
                .setValue(applicationIncidentEvent)
                .build();

        try {
            producer.produce(accountEntryMessage, new ProduceCallback<Application, ApplicationIncidentEvent>() {
                @Override
                public void onComplete(ProducedMessage producedMessage) {
                    LOG.info(">>>> Produced an application incident event on topic {} and partition {} with offset {}", producedMessage.getStream(),
                            producedMessage.getPartition(),
                            producedMessage.getOffset());
                }

                @Override
                public void onError(ProducerMessage producerMessage, ExecutionException e) {
                    LOG.warn("Something went wrong while trying to produce the message");

                }
            }).get();
        } catch (InterruptedException | ExecutionException e) {
            LOG.error("Error producing ", e);
        }
    }

    private static SpecificAvroProducerConfig<Application, ApplicationIncidentEvent> getAvroProducerConfig() {
        return SpecificAvroProducerConfig.<Application, ApplicationIncidentEvent>builder()
                .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                .setOrderingStrategy(OrderingStrategy.KEEPING_ORDER)
                .build();
    }

    private static ClientConfig getClientConfig() {
        return ClientConfig.newBuilder()
                // This is the app ID as it is configured in the self service
                .setApplicationId("incident.reporter")
                .setApplicationVersion("1.0.0")
                // The endoint of the Discovery API (used to retrieve information about bootstrap servers, schema registry, TTL etc...)
                .setEndpoint("http://127.0.0.1:8081/")
                // The environment corresponding to the shortname of the env in the self service
                .setEnvironment("local")
                // The tenant you are part of
                .setTenant("axual")
                // The ssl configuration for your application. The certificate used should have a DN
                // matching the DN that was configured in self service
                .setSslConfig(SslConfig.newBuilder()
                        .setKeystoreType(SslConfig.KeystoreType.PEM)
                        .setKeystoreCertificateChain("-----BEGIN CERTIFICATE-----\nMIIDkjCCAnoCCQD8145Kq9TyjDANBgkqhkiG9w0BAQUFADCBhTELMAkGA1UEBhMC\nTkwxFjAUBgNVBAgMDU5vcnRoIEJyYWJhbnQxFjAUBgNVBAcMDVJhYW1zZG9ua3Zl\nZXIxDjAMBgNVBAoMBUF4dWFsMR0wGwYDVQQLDBRQbGF0Zm9ybSBEZXZlbG9wbWVu\ndDEXMBUGA1UEAwwObG9jYWwuYXh1YWwuaW8wHhcNMTgwNjI2MTcyMTA3WhcNMjgw\nNjIzMTcyMTA3WjCBjzELMAkGA1UEBhMCTkwxFjAUBgNVBAgTDU5vcnRoIEJyYWJh\nbnQxFjAUBgNVBAcTDVJhYW1zZG9ua3ZlZXIxDjAMBgNVBAoTBUF4dWFsMR0wGwYD\nVQQLExRQbGF0Zm9ybSBEZXZlbG9wbWVudDEhMB8GA1UEAxMYQXh1YWwgUGxhdGZv\ncm0gRGV2ZWxvcGVyMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsSoK\ns7ADOa8FX4qU4V3SdwoSvxoTa7u9CEcyNzx5TZ2iUrAh5r+Gb/RgI7exVrBXNbSq\nRCdNtJLnpJ+43buuJIJf/enAuPMnMrfS3lba1O31pfPM9+5imK83I5nFwsAgJXTi\n0WmCTkjLRE1QF0PflHBRfseN+Br9K44xqJvzPQEj7a3ZVv4W1XhKSYf5P6R5/s9+\n+KrI+/a4pvJB4VkyPEO9fXf1eK6e4BCSfGC5TwrRhDTQPrrlFVRf7PT/DoDeZrVu\nk9L09qOJyfrOzIsmVH8IE1h3ilrJDiMOd+sdc85e9KfuVbPIM4tepjzzHOctpUAn\n93Wl0Tk7Qn5NPdusKwIDAQABMA0GCSqGSIb3DQEBBQUAA4IBAQAQXfxTJkm2pVH1\naxuebosQbUw5KYYVWcMX/3MPywbqnEcn0bRvr/p0u3652Ajv5nLKk18zG/3fOhaa\nHuBTP1TILExVHxZwG5dte/DWX2HaVqx6z1y8Y9Y89B0LazFmrD0ToiI6EusmHzKY\nbLXg2oouNprhh7q980ksSuy5fazBll0RlOsvyz7rDBwuOqEATjB6TCkxaRd4pgZI\nDknnrE6ySsvBggreAtaLa1AC+tYH1Lzw05Rwkqh5hFKygi6Ay5/5eFAA2cLVxZkc\nYpzPkS5S0VtraOj7M42cvnv8WCnpwu1OIYyH1uMculTRZ3FMztob5hXsuR5afV19\n/wpgAW75\n-----END CERTIFICATE-----\n-----BEGIN CERTIFICATE-----\nMIIDiDCCAnACCQDF8fbaJl/f3DANBgkqhkiG9w0BAQsFADCBhTELMAkGA1UEBhMC\nTkwxFjAUBgNVBAgMDU5vcnRoIEJyYWJhbnQxFjAUBgNVBAcMDVJhYW1zZG9ua3Zl\nZXIxDjAMBgNVBAoMBUF4dWFsMR0wGwYDVQQLDBRQbGF0Zm9ybSBEZXZlbG9wbWVu\ndDEXMBUGA1UEAwwObG9jYWwuYXh1YWwuaW8wHhcNMTgwNjI2MTcxOTIwWhcNMjgw\nNjIzMTcxOTIwWjCBhTELMAkGA1UEBhMCTkwxFjAUBgNVBAgMDU5vcnRoIEJyYWJh\nbnQxFjAUBgNVBAcMDVJhYW1zZG9ua3ZlZXIxDjAMBgNVBAoMBUF4dWFsMR0wGwYD\nVQQLDBRQbGF0Zm9ybSBEZXZlbG9wbWVudDEXMBUGA1UEAwwObG9jYWwuYXh1YWwu\naW8wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCdmUKbxLc59vNCKKOa\nieFo26uGRD7Q1AKmFyb6ZbnhkxKNqMs+DWCRwSS5b/Tw9yQ8jc3iuXor3vyHK3pu\n5LvDINBbwXOEHbEnZrqxQXK5fLy3Jm+XhO5IL1V2GGAUYvrousDzlhmrwnc1i0Bt\nX2jGkDgP3if1G+fdmobrDgL7bDJ59l33F7yrdgkaiNwPDuApAbWt+ZyeQDZ3oNLG\nEnMOQTv4iO2VHVziB/J0tj1Bixt+jVgriFpgptYQmcjRLPo1LZln3fdp3NtoaMe6\nYUYHHUV5TAfU23EQ24VJvgOUs+A/iplXgsWdOfPnmr9EXj+QZFPlZJ2wUz/1/z+G\nSh9BAgMBAAEwDQYJKoZIhvcNAQELBQADggEBABASw+75Bo+zPeTU7KLjws+Tzpvz\nYHzBbTOjJFz+IXfOKatbMh3pjepLmmABNdlQ2L6aqO/0qFfGv2abWvF/+ok5zWNn\nN/peZxcxe3u7OUrkCJuUEHYX0TgltR9bRbnbnqjLUq4vKzQ06KKFbW0HTbtC2vk7\nJPUDviqinjEjI2CsafiZAf0grXEzmD8nj+SNafrWf8F3pyivn6FuQ0XbBVptMZrS\numc8YqsHJXhdA6MYQGapMnv9GYG+Ka0SbuwmkFy0kRpDOU8MQy0tYK3WszoSRT+O\nDb4XaRAufP985vahvC6aSR7LnvRezNgADfmKbZl5jVFXG/MV/JyS6mtm8GA=\n-----END CERTIFICATE-----")
                        .setKeystoreKey("-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCxKgqzsAM5rwVf\nipThXdJ3ChK/GhNru70IRzI3PHlNnaJSsCHmv4Zv9GAjt7FWsFc1tKpEJ020kuek\nn7jdu64kgl/96cC48ycyt9LeVtrU7fWl88z37mKYrzcjmcXCwCAldOLRaYJOSMtE\nTVAXQ9+UcFF+x434Gv0rjjGom/M9ASPtrdlW/hbVeEpJh/k/pHn+z374qsj79rim\n8kHhWTI8Q719d/V4rp7gEJJ8YLlPCtGENNA+uuUVVF/s9P8OgN5mtW6T0vT2o4nJ\n+s7MiyZUfwgTWHeKWskOIw536x1zzl70p+5Vs8gzi16mPPMc5y2lQCf3daXROTtC\nfk0926wrAgMBAAECggEAaAC/QZcdfZqCdAD9v4N9j0ZJlQgwyHjw0tBA6W5F48ub\nRCGD9VsQB98VJUKsB7EDsVJ69gGAu3XWKK1fMEQCSgqDYaL88VZE96A0WTPxyThc\nkeyash2uoeWSYALgtqBk/rgsgzUGOwC+2zzrvIyqzxBUtzFc5X6qiwwxmMLcOz3Y\nv+bP0KcsCl/uHHpjgPYqQhpCl1G8ErbMA8sgyzb+X6RhxHrz22+1jkxgL/cGUubb\n+g1HyxdUAKKK8R1MTgsYVW6XOOFWPCn7hDmFzZtckxTdrA+66rM/OmZLRoww9vQZ\nerGiH1Uv7GEdBYhYqseEJZ1PxvK3O6YC78jcffq9uQKBgQD0Lw9vZo1hDHG7Lmt0\nXqAMtOwY17CQ4tQRrKkFPboqSqKThdXQ9P+sjo1YU6wGNTW6NiI+5zcEzC6sWatc\nRrZKfBra0E1+n5vCT/yYepfF+3xC4wfFhEuZPFGQIdh0XGmhkT0DukfqLWH2XiG+\nOHvUxq+EN/ITCdN50zZidXBjJQKBgQC5vL7LTep+dBZYg31ZMx3j70OGYIH/lttz\nDW3BwdNBJp+vG8Q8TnsSZ25v0kaB1cYdGDTge3WW3ZTaXSfWWnutuU4BTI6eD6UH\ncLJ0vZd04Qk00ttbzgE6JiQm4HrhBHZgcJQbJEkogV+6Y0/mAFu7DG2XH6r6LFnB\nKBWT4DZZDwKBgCgpUVWWPWyX2mDZ+qxyH8rXOvm/B/hchlq91jLZezQXgHPZEFjE\n4wRjkdXUNTf0KnkNDEbiSodMeeS4/tk3fCX2EYipuAU6hSjJdRczGqFigoaRxqZy\n4ug6JoQZPPuuc2UyeSGS0t8uRa16v/wEWEGfyCBr/zGobRLdbVV2UVzNAoGAOSV9\nfofmkimdhnZOZtd3Zt4C5KFk3gLIWknTbz33haAgmXvtkLCE5VC1heooj2H6ppEA\nE+FoeJaMafMngqgsTXMqMPQhHTirCfL+tTRwGSHz9zC5FTH45q89iEihBgKdeWap\n6v/rEm9byLktqBKMJqzYOxsfPAHRS8DNgsYFcrkCgYA/JmRzs6jhdpn9Q6aKGejz\naLhdvmSio1xL523FGARIqYJHFjBse9lcg/Khd1pUJpCFWkDdMLzPzw1Hi2wJEWZo\nxqHQBqF0YW+ylzIMdTh4mdnOwRnDRBh2rmpVwJCX9+5cwzjAiEW+xitz9hVfPAQx\nvDUOeh3W/XCcwh/Y2R9Klw==\n-----END PRIVATE KEY-----")
                        .setTruststoreType(SslConfig.TruststoreType.PEM)
                        .setTruststoreCertificates("-----BEGIN CERTIFICATE-----\nMIIDiDCCAnACCQDF8fbaJl/f3DANBgkqhkiG9w0BAQsFADCBhTELMAkGA1UEBhMC\nTkwxFjAUBgNVBAgMDU5vcnRoIEJyYWJhbnQxFjAUBgNVBAcMDVJhYW1zZG9ua3Zl\nZXIxDjAMBgNVBAoMBUF4dWFsMR0wGwYDVQQLDBRQbGF0Zm9ybSBEZXZlbG9wbWVu\ndDEXMBUGA1UEAwwObG9jYWwuYXh1YWwuaW8wHhcNMTgwNjI2MTcxOTIwWhcNMjgw\nNjIzMTcxOTIwWjCBhTELMAkGA1UEBhMCTkwxFjAUBgNVBAgMDU5vcnRoIEJyYWJh\nbnQxFjAUBgNVBAcMDVJhYW1zZG9ua3ZlZXIxDjAMBgNVBAoMBUF4dWFsMR0wGwYD\nVQQLDBRQbGF0Zm9ybSBEZXZlbG9wbWVudDEXMBUGA1UEAwwObG9jYWwuYXh1YWwu\naW8wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCdmUKbxLc59vNCKKOa\nieFo26uGRD7Q1AKmFyb6ZbnhkxKNqMs+DWCRwSS5b/Tw9yQ8jc3iuXor3vyHK3pu\n5LvDINBbwXOEHbEnZrqxQXK5fLy3Jm+XhO5IL1V2GGAUYvrousDzlhmrwnc1i0Bt\nX2jGkDgP3if1G+fdmobrDgL7bDJ59l33F7yrdgkaiNwPDuApAbWt+ZyeQDZ3oNLG\nEnMOQTv4iO2VHVziB/J0tj1Bixt+jVgriFpgptYQmcjRLPo1LZln3fdp3NtoaMe6\nYUYHHUV5TAfU23EQ24VJvgOUs+A/iplXgsWdOfPnmr9EXj+QZFPlZJ2wUz/1/z+G\nSh9BAgMBAAEwDQYJKoZIhvcNAQELBQADggEBABASw+75Bo+zPeTU7KLjws+Tzpvz\nYHzBbTOjJFz+IXfOKatbMh3pjepLmmABNdlQ2L6aqO/0qFfGv2abWvF/+ok5zWNn\nN/peZxcxe3u7OUrkCJuUEHYX0TgltR9bRbnbnqjLUq4vKzQ06KKFbW0HTbtC2vk7\nJPUDviqinjEjI2CsafiZAf0grXEzmD8nj+SNafrWf8F3pyivn6FuQ0XbBVptMZrS\numc8YqsHJXhdA6MYQGapMnv9GYG+Ka0SbuwmkFy0kRpDOU8MQy0tYK3WszoSRT+O\nDb4XaRAufP985vahvC6aSR7LnvRezNgADfmKbZl5jVFXG/MV/JyS6mtm8GA=\n-----END CERTIFICATE-----")
                        .setEnableHostnameVerification(false)
                        .build())
                .build();
    }
}
