package com.demo.operations;

import lombok.extern.slf4j.Slf4j;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.scheduling.annotation.EnableScheduling;


@EnableScheduling
@Slf4j
@SpringBootApplication
public class IncidentReporterCommandLineRunner {

    public static void main(String... args) throws Exception {
        new SpringApplicationBuilder(IncidentReporterCommandLineRunner.class).web(false).run(args);
    }
}
