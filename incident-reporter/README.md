# Incident reporter

This demo application simulates incidents randomly happening in applications. 

Good to know:
- The [IncidentEventGenerator](src/main/java/com/demo/operations/generator/IncidentEventGenerator.java) class simulates incidents with a random severity
- The [Application](src/schemas/general/Application.avsc) schema is used for the key of the messages
- The [ApplicationIncidentEvent](src/schemas/general/ApplicationIncidentEvent.avsc) schema is used for the value of the messages.

How to start:
```bash
mvn clean package spring-boot:run
```

