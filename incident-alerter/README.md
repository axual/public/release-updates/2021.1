# Incident alerter

The incident alerter only alerts upon CRITICAL `ApplicationIncidentEvents` and it does so by only consuming messages from the `application-incidents-severe` topic with `isolation.level=READ_COMMITTED`.
