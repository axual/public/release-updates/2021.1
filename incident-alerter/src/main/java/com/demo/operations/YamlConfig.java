package com.demo.operations;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.axual.client.proxy.generic.registry.ProxyChain;
import lombok.extern.slf4j.Slf4j;

/**
 * Config for the incident analyzer, backed by a yaml file on the classpath.
 * Note: very little checking on the passed yaml, (spelling) mistakes and/or omissions will
 * result in an error.
 */
@Slf4j
public class YamlConfig {

    private final String resourceName;

    private Map<String, Object> config;

    /**
     * Create a new instance, based on the provided YAML resource name.
     * @param resourceName
     */
    public YamlConfig(String resourceName) {
        this.resourceName = resourceName;
        log.info("init({})", resourceName);
        final ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        try (InputStream stream = getClass().getClassLoader().getResourceAsStream("config.yaml")) {
            config = mapper.readValue(stream, Map.class);
        } catch (IOException e) {
            log.error("Failed to parse config.yaml: {}", e.getMessage());
            config = new HashMap<>();
        }
    }

    /**
     * Get a producer config from the yaml.
     * @return
     */
    public Map<String, Object> getConsumerConfig() {
        Map<String, Object> result = (Map) config.get("consumer");
        // vonvert list of proxies to proxy chain
        if (result.containsKey("axualconsumer.chain")) {
            final List<String> proxies = (List<String>) result.get("axualconsumer.chain");
            result.put("axualconsumer.chain", createProxychain(proxies));
        }

        // instantiate serializers and deserializers
        String keyser = (String) result.get("key.deserializer");
        if (keyser != null) {
            result.put("key.deserializer", newInstance(keyser));
        }
        String valueser = (String) result.get("value.deserializer");
        if (valueser != null) {
            result.put("value.deserializer", newInstance(valueser));
        }

        return result;
    }

    /**
     * Create a proxy chain, based on a list of proxy names. The names are not checked for validity.
     * @param proxies
     * @return
     */
    private ProxyChain createProxychain(List<String> proxies) {
        final ProxyChain.Builder builder = ProxyChain.newBuilder();
        for (String proxy: proxies) {
            builder.append(proxy);
        }
        return builder.build();
    }

    /**
     * Return a new instance of the specified class, using the no-arg constructor.
     * In case of any error, return <code>null</code>.
     * @param className
     * @return
     */
    private Object newInstance(String className) {
        try {
            final Constructor<?> constructor = Class.forName(className).getConstructor();
            return constructor.newInstance();
        } catch (NoSuchMethodException | ClassNotFoundException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            log.error("Failed to instantiate {}: {}({})", className, e.getClass().getSimpleName(), e.getMessage());
            return null;
        }
    }
}
