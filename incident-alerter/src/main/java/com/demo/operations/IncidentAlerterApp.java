package com.demo.operations;

import java.util.Map;

import lombok.extern.slf4j.Slf4j;

import static java.lang.Thread.sleep;

@Slf4j
public class IncidentAlerterApp {

    public static void main(String[] args) throws InterruptedException {
        YamlConfig yamlConfig = new YamlConfig("config.yml");
        final Map<String, Object> consumerConfig = yamlConfig.getConsumerConfig();

        try (TransactionalIncidentAlerter consumer = new TransactionalIncidentAlerter(consumerConfig, "application-incidents-filtered")) {
            while (consumer.isConsuming()) {
                sleep(100);
            }
        }
    }
}
