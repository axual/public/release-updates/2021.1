package com.demo.operations;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;

import io.axual.client.proxy.axual.consumer.AxualConsumer;

public class TransactionalIncidentAlerter implements AutoCloseable, Runnable {
    private static final Logger log = LoggerFactory.getLogger(TransactionalIncidentAlerter.class);
    private final AxualConsumer<Application,ApplicationIncidentEvent> consumer;

    private final Future<?> runner;
    private final AtomicBoolean continueRunning;
    private final Map<String,ApplicationIncidentEvent> consumedIncidents = new HashMap<>();

    public TransactionalIncidentAlerter(Map<String, Object> consumerConfig, String topic) {
        this.consumer = new AxualConsumer<>(consumerConfig);

        // Subscribe the consumer to the topic
        this.consumer.subscribe(Collections.singleton(topic));

        this.continueRunning = new AtomicBoolean(true);
        this.runner = Executors.newFixedThreadPool(1).submit(this);
    }

    @Override
    public void close() {
        if (!this.consumer.subscription().isEmpty()) {
            log.info("Unsubscribing consumer");
            this.consumer.unsubscribe();
        }

        log.info("Closing consumer");
        this.consumer.close();
    }

    boolean isConsuming() {
        return !this.runner.isDone();
    }

    @Override
    public void run() {
        while (this.continueRunning.get()) {
            ConsumerRecords<Application, ApplicationIncidentEvent> records = consumer.poll(Duration.ofMillis(3000));
            if (records.isEmpty()) {
                continue;
            }

            log.debug("Polled {} records, start transaction", records.count());

            for (ConsumerRecord<Application, ApplicationIncidentEvent> record : records) {
                log.debug("Processing partition {}, offset {}, severity {}", record.partition(), record.offset(), record.value().getSeverity());
                log.info("Alerting the operator for incident with ID {}", record.value().getIncidentId());
                if (consumedIncidents.containsKey(record.value().getIncidentId().toString())){
                    log.warn("-----------------------------------");
                    log.warn("Warning, this is a duplicate alert!");
                    log.warn("-----------------------------------");
                } else {
                    log.info("Everything OK, {} is a first time alert.",record.value().getIncidentId());
                    consumedIncidents.put(record.value().getIncidentId().toString(), record.value());
                }
                // Commit right away to prevent issues in the alerter app to cause duplicates
                consumer.commitSync();
            }
        }
        log.info("End of run loop, unsubscribe and close");
        this.close();
    }
}
