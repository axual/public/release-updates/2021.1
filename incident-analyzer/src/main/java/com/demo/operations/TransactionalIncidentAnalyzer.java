package com.demo.operations;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.TopicPartition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.StreamSupport;

import io.axual.client.proxy.axual.consumer.AxualConsumer;
import io.axual.client.proxy.axual.producer.AxualProducer;

public class TransactionalIncidentAnalyzer implements AutoCloseable, Runnable {
    private static final Logger log = LoggerFactory.getLogger(TransactionalIncidentAnalyzer.class);
    private final Consumer<Application, ApplicationIncidentEvent> consumer;
    private final Producer<Application, ApplicationIncidentEvent> producer;

    private final Map<TopicPartition, OffsetAndMetadata> successRecords = new HashMap<>();
    private final Future<?> runner;
    private final AtomicBoolean continueRunning;

    public TransactionalIncidentAnalyzer(Map<String, Object> consumerConfig, Map<String, Object> producerConfig, String topic) {
        this.consumer = new AxualConsumer<>(consumerConfig);
        this.producer = new AxualProducer<>(producerConfig);

        // Initialize transactions, this happens only once in the lifetime of a producer
        producer.initTransactions();

        // Subscribe the consumer to the topic
        this.consumer.subscribe(Collections.singleton(topic));

        this.continueRunning = new AtomicBoolean(true);
        this.runner = Executors.newFixedThreadPool(1).submit(this);
    }

    @Override
    public void close() {
        if (!this.consumer.subscription().isEmpty()) {
            log.info("Unsubscribing consumer");
            this.consumer.unsubscribe();
        }

        log.info("Closing consumer");
        this.consumer.close();

        log.info("Closing producer");
        this.producer.close();
    }

    @Override
    public void run() {
        log.info("Reading from topic");
        Map<TopicPartition, Long> beginOffsets = new HashMap<>();
        while (this.continueRunning.get()) {

            // Set begin positions
            beginOffsets.clear();
            successRecords.clear();
            consumer.assignment().forEach(tp -> beginOffsets.put(tp, consumer.position(tp)));

            // Poll for new records on the topic
            ConsumerRecords<Application, ApplicationIncidentEvent> records = consumer.poll(Duration.ofMillis(3000));

            if (records.isEmpty()) {
                // No records fetched, restart loop
                continue;
            }

            log.info("Polled {} records, start transaction", records.count());

            producer.beginTransaction();
            // Handle consumed message
            boolean success = StreamSupport.stream(records.spliterator(), false)
                    .peek(record -> log.info("Processing record on partition {}, offset {} with incident ID {} and severity {}", record.partition(), record.offset(), record.value().getSeverity(),record.value().getSeverity()))
                    .filter(record -> record.value().getSeverity().equals(Severity.CRITICAL))
                    .allMatch(this::processMessage);   // Will stop processing the stream on first false

            // Abort transaction cancels active batches of producer
            // Forces producer to broker to illustrate committed and uncommitted data.
            producer.flush();

            if (success) {
                log.info("Processing succeeded: committing transaction");
                producer.sendOffsetsToTransaction(successRecords, consumer.groupMetadata());
                producer.commitTransaction();
            } else {
                log.warn("Processing failed: aborting transaction and resetting position");
                producer.abortTransaction();
                beginOffsets.forEach(consumer::seek);
            }
        }
        log.info("End of run loop, unsubscribe and close");
        this.close();
    }

    // This method simulates the DB write operation, it randomly fails 10% of the time

    private boolean processMessage(ConsumerRecord<Application, ApplicationIncidentEvent> record) {
        // Produce message regardless of whether processing fails or not
        ProducerRecord<Application, ApplicationIncidentEvent> producerRecord = new ProducerRecord<>("application-incidents-filtered", record.key(), record.value());
        producer.send(producerRecord);

        // Randomly fail 10% of attempts
        Random random = new Random();
        boolean succeeded = random.nextInt(10) < 9;
        if (succeeded) {
            log.info("Message processed successfully");
            successRecords.put(new TopicPartition(record.topic(), record.partition()), new OffsetAndMetadata(record.offset() + 1, "no metadata"));
        } else {
            log.warn("----------------------------------");
            log.warn("Message NOT processed successfully");
            log.warn("----------------------------------");
        }
        return succeeded;
    }

    boolean isConsuming() {
        return !this.runner.isDone();
    }

}
