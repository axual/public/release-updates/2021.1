package com.demo.operations;

import java.util.Map;

import lombok.extern.slf4j.Slf4j;

import static java.lang.Thread.sleep;

@Slf4j
public class IncidentAnalyzerApp {

    public static void main(String[] args) throws InterruptedException {
        // Load the consumer and producer configurations from the config.yaml file on the classpath
        YamlConfig yamlConfig = new YamlConfig("config.yml");
        final Map<String, Object> producerConfig = yamlConfig.getProducerConfig();
        final Map<String, Object> consumerConfig = yamlConfig.getConsumerConfig();

        try (TransactionalIncidentAnalyzer consumer = new TransactionalIncidentAnalyzer(consumerConfig, producerConfig, "application-incidents")) {
            while (consumer.isConsuming()) {
                sleep(100);
            }
        }
    }
}
