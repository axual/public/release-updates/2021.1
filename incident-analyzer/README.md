# Incident analyzer

The incident analyzer applications reads Application Incident events from the `application-incidents` topic and uses the transactional features of Kafka to make sure only messages that were successfully processed are being marked as such, and part of a committed transaction.

## How to run

```bash
mvn exec:java
```

